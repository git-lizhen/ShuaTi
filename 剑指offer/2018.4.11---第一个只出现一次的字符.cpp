//问题描述：在一个字符串(1<=字符串长度<=10000，全部由字母组成)中找到第一个只出现一次的字符,并返回它的位置

//解题思路：利用map解决，map<char,int>

int FirstNotRepeatingChar(string str) {
    map<char,int> temp;
    for(int i=0;i<str.size();++i)
        temp[str[i]]++;
    for(int i=0;i<str.size();++i)
        if(temp[str[i]]==1)
            return i;
    return -1;
}