//问题描述：输入一个正整数数组，把数组里所有数字拼接起来排成一个数，打印能拼接出的所有数字中最小的一个。
//例如输入数组{3，32，321}，则打印出这三个数字能排成的最小数字为321323。

//解题思路：

//将数组按一下规则进行排序：
//首先将正数元素利用to_string转化为string
//然后比较两个string元素a、b的和:
//若a+b>b+a,表明a>b,b应排在前;
//否则，a<=b，a应排在前.

bool cmp(const int &a,const int &b) const
{
    string ab="",ba="";
    ab+=to_string(a)+to_string(b);
    ba+=to_string(b)+to_string(a);
    return ab<ba;
}

string PrintMinNumber(vector<int> numbers) {
    if(numbers.size()==0)
        return "";
    if(numbers.size()==1)
        return to_string(numbers[0]);
    string res="";
    sort(numbers.begin(),numbers.end(),cmp);
    for(int i=0;i<numbers.size();++i)
        res+=to_string(numbers[i]);
    return res;
}