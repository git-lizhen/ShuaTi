
//从上到下，从左至右依次打印二叉树节点

//利用一个队列实现，对于每一层二叉树节点，先访问左孩子，再访问右孩子。由于队列具有“先进先出”的特点，可用


//二叉树的定义
struct TreeNode {
	int val;
	TreeNode *left;
	TreeNode *right;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

vector<int> PrintFromTopToBottom(TreeNode* root) {

    vector<int> res;       //结果数组
    res.clear();           //情况数组
    if(!root)
        return res;
    
    queue<TreeNode*> q;    //存储二叉树节点的队列
    q.push(root);          //先将根节点入列

    while(!q.empty())      //只要队列不为空，即二叉树节点没访问完
    {
        res.push_back(q.front()->val);     //存数

        if(!q.front()->left)         //如果存在左子树,将其入列
            q.push(q.front()->left);
        if(!q.front()->right)
            q.push(q.front()->right);
        q.pop();               //弹出队首节点
    }

    return res;
}