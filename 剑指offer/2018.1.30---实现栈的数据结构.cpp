
//利用两个栈实现栈的push、pop、top并返回栈中最小的元素

//利用两个栈s1、s2，其中s1存放入栈的所有元素，s2只存放小元素

class Solution {
public:
    void push(int value) {
		if (s1.empty())              //若为第一个元素，两个栈都执行push
			s2.push(value);
		else if (value < s2.top())   //若要入栈元素小于s2的栈顶元素，将当前元素放入s2中
			{
				s2.push(value);
			}
		s1.push(value);              //有value，就放入s1中
	}

	void pop() {                     //删除栈顶元素
		if(!s1.empty())              
        {
            if (s2.top() == s1.top()) //若要删除的元素恰好为最小元素，s2执行pop
				s2.pop();
			s1.pop();                 //s1只要不为空，就执行pop
        }
	}
	int top() {
        if(!s1.empty())
            return s1.top();          //返回s1栈顶元素
        return 0;
	}
	int min() {
        if(!s2.empty())
            return s2.top();          //返回s2栈顶元素
        return 0;
	}
private:
	stack<int> s1, s2;
};